require('./config/config');
require('./models/db');
require('./middlewares/passportConfig');
require('./MQTT/MQTTConection');

const express = require('express');
const path = require('path');
const rtsIndex = require('./routes/index');
const app = express();
const bodyParser = require('body-parser');
const passport = require('passport');
const http = require('http');
const cors = require('cors');



// middleware
// parse application/json
app.use(bodyParser.json());
app.use(cors());
app.use(passport.initialize(undefined));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// Public folder
// app.use('/', express.static(path.resolve(__dirname, 'web')));
app.use(rtsIndex);
// app.get('*', (req, res) => { res.sendFile(path.join(__dirname, 'web/index.html'))});

// error handler
app.use((err, req, res, next) => {
    if (err.name === 'ValidationError') {
        var valErrors = [];
        Object.keys(err.errors).forEach(key => valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors)
    }
});

const server = http.createServer(app);

// start server
server.listen(process.env.PORT, () => console.log(`Server started at port : ${process.env.PORT}`));
