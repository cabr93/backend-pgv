// check env.
const env = process.env.NODE_ENV || 'development';
// fetch env. config
const config = require('./config.json');
const envConfig = config[env];
// add env. config values to process.env
Object.keys(envConfig).forEach(key => process.env[key] = envConfig[key]);

// MQTT server
process.env.MQTT = process.env.MQTT || 'mqtt://192.168.0.211';
process.env.MQTTUser = process.env.MQTTUser || 'SIMSRD';
process.env.MQTTPws = process.env.MQTTPws || 'U2ltczIwMTkq';