const {newPoint} = require("../controllers/coverage.controller");
const { client } = require('./MQTTConection');
const topic = 'sensors/livingroom/temp';


client.on('connect', function () {
    client.subscribe(topic, function (err) {
        if (!err) {
            client.publish('presence', 'Hello mqtt')
        } else {
            console.log("AAA")
        }
    })
});

client.on('message', function (topic, message) {
    // message is Buffer
    newPoint(message);
});

