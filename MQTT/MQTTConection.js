const MQTTConnection = require('mqtt');

// MQTT Connection
let client = MQTTConnection.connect(process.env.MQTT,{
    username: process.env.MQTTUser,
    password: process.env.MQTTPws,
    clean:true});

module.exports = {
    client
};

require('./subscribeGateways');
