const mongoose = require('mongoose');
const User = mongoose.model('User');
const sistema = mongoose.model('sistema');

let icons = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if ( user.roles.icons) {
                sistema.find({}).then((systems)=>{
                    res.send(systems);
                });
            }
            else res.status(400).json({message:'Error al obtener los iconos'});
        }
    )
};

let edit = (req, res) => {
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if ( user.roles.icons) {
                sistema.findOne({name: req.body.tipo},
                    (err, marker) => {
                        if (!marker) return res.status(404).json({ status: false, message: 'User record not found.' });
                        else
                            marker.url =req.body.url;
                            marker.id = req.body.id;
                            marker.save((err,doc)=>{
                                if (err) res.status(400).send({message:'Error al cambiar la clave'});
                                else res.status(200).json({message:'Procedimiento correcto'});
                        })
                    }
                )
            }
            else res.status(400).json({message:'Error al obtener los markers'});
        }
    )
};

module.exports = {
    icons,
    edit
};
