const mongoose = require('mongoose');
const coverage = mongoose.model('coverage');
const User = mongoose.model('User');


let newPoint = (message) => {
    let ObjMessage = JSON.parse(message.toString());
    if( ObjMessage.hasOwnProperty('payload')) {
        let bit3 = ObjMessage.payload[3].toString(16).length === 1?'0'+ObjMessage.payload[3].toString(16):ObjMessage.payload[3].toString(16);
        let bit4 = ObjMessage.payload[4].toString(16).length === 1?'0'+ObjMessage.payload[4].toString(16):ObjMessage.payload[4].toString(16);
        let bit5 = ObjMessage.payload[5].toString(16).length === 1?'0'+ObjMessage.payload[5].toString(16):ObjMessage.payload[5].toString(16);
        let bit6 = ObjMessage.payload[6].toString(16).length === 1?'0'+ObjMessage.payload[6].toString(16):ObjMessage.payload[6].toString(16);
        let bit7 = ObjMessage.payload[7].toString(16).length === 1?'0'+ObjMessage.payload[7].toString(16):ObjMessage.payload[7].toString(16);
        let bit8 = ObjMessage.payload[8].toString(16).length === 1?'0'+ObjMessage.payload[8].toString(16):ObjMessage.payload[8].toString(16);
        let bit9 = ObjMessage.payload[9].toString(16).length === 1?'0'+ObjMessage.payload[9].toString(16):ObjMessage.payload[9].toString(16);
        let bit10 = ObjMessage.payload[10].toString(16).length === 1?'0'+ObjMessage.payload[10].toString(16):ObjMessage.payload[10].toString(16);
        let lat = parseInt(`${bit3}${bit4}${bit5}${bit6}`, 16)*90/2147483647;
        let lng = parseInt(`${bit7}${bit8}${bit9}${bit10}`, 16)*180/2147483647;
        if (lng>180) lng= (360-lng)*-1;
        if (lat !== 0) {
            let newPointCoverage = new coverage({
                lat,
                lng,
                devEui: ObjMessage.deveui,
                gwEui: ObjMessage.gweui,
                rssi: ObjMessage.rssi,
                lSnr: ObjMessage.lsnr,
                msgId: ObjMessage._msgid,
                freq: ObjMessage.freq,
                time: ObjMessage.time,
                payload: ObjMessage.payload.toString()
            });
            newPointCoverage.save((err) => {
                if (err)
                    console.log(err);
                else{
                    console.log(newPointCoverage)
                }
            });
        }
    }
};

let coveragePoint = (req, res) => {
    User.findById({_id:req._id},
        (err,user) => {
            if (err) res.status(400).json({message:'Error al encontrar usuario'});
            else if (!user) res.status(400).json({message:'Error al encontrar usuario'});
            // colocar condicion de rol como : user.roles.alerts
            else if (user.roles.coverage) {
                coverage.find({deleted:{$in: [false, null]}}).then((points)=>{
                    res.send(points);
                });
            }
            else res.status(400).json({message:'Error al encontrar puntos de covertura'});
        }
    )
};
let coverageGateways = (req, res) => {
    User.findById({_id:req._id},
        (err,user) => {
            if (err) res.status(400).json({message:'Error al encontrar usuario'});
            else if (!user) res.status(400).json({message:'Error al encontrar usuario'});
            // colocar condicion de rol como : user.roles.alerts
            else if (user.roles.coverage) {
                coverage.find({deleted:{$in: [false, null]}}).distinct('gwEui').then((points)=>{
                    res.send(points);
                });
            }
            else res.status(400).json({message:'Error al encontrar puntos de covertura'});
        }
    )
};

module.exports = {
    newPoint,
    coveragePoint,
    coverageGateways
};
