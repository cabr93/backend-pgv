const mongoose = require('mongoose');
const tracking = mongoose.model('rastreo');
const User = mongoose.model('User');
const sistema = mongoose.model('sistema');

let devices = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.tracking) {
                tracking.find({}).then((devices)=>{
                    res.send(devices);
                });
            }
            else res.status(400).json({message:'Error al encontrar dispositivo de rastreo'});
        }
    )
};

let maps = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.tracking) {
                sistema.find({}).then((systems)=>{
                    let urlTracking = systems.filter((key) => key.name === 'Rastréo')[0].url;
                    tracking.find({}).then((devices)=>{
                        let final=[];
                        let url = '';
                        devices.forEach((device)=>{
                            if(device.state) url=`${urlTracking}_on.svg`;
                            else url=`${urlTracking}_off.svg`;
                            let date1 = new Date(device.lastubication);
                            let popOver = {
                                name:device.placa,
                                lat:device.lat,
                                long:device.long,
                                message:`<h4><strong>${device.placa}</strong></h4><h5><strong>Dispositivo:</strong> GPS</h5><h5><strong>Ultima ubicacion:</strong>${date1.toLocaleString('en-GB')} </h5><h5><strong>Velocidad:</strong>${device.speed} km/h</h5>`,
                                type: 'Rastréo',
                                state:device.state,
                                key:'tracking',
                                url:url,
                                id:device._id
                            };
                            final.push(popOver);
                        });
                        res.send(final);
                    });
                });
            }
            else{
                res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
            }
        }
    )
};

module.exports = {
    devices,
    maps
};

