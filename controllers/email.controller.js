const mongoose = require('mongoose');
const User = mongoose.model('User');
const email = mongoose.model('email');

let getEmail = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.roles.geofence) {
                email.find({}).then((todos)=>{
                    res.send(todos);
                });
            }
            else res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
        }
    )
};

let edit = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.lora && user.roles.devices) {
                email.find({}).then((todos)=>{
                    email.findByIdAndUpdate(todos[0]._id,req.body, { new: true, runValidators: true, context: 'query'},(err, email)=>{
                        if (err) {
                            res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
                        }
                        res.status(200).json({message:'Procedimiento correcto'});
                    })
                });
            }
            else res.status(400).json({message:'Error al editar dispositivo LoRa'});
        }
    )
};

module.exports = {
    edit,
    getEmail
};
