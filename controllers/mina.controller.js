const mongoose = require('mongoose');
const mina = mongoose.model('mina');
const User = mongoose.model('User');
const ctrlLog  = require('../controllers/log.controller');
const sistema = mongoose.model('sistema');

let devices = (req, res) =>{
    User.findById({_id:req._id},
        (err,user) => {
            if (err) res.status(400).json({message:'Error al encontrar usuario'});
            else if (!user) res.status(400).json({message:'Error al encontrar usuario'});
            else if (user.services.mina) {
                mina.find({deleted:{$in: [false, null]}}).then((devices)=>{
                    res.send(devices);
                });
            }
            else res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
        }
    )
};

let edit = (req, res) => {
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.mina && user.roles.devices) {
                const myQuery = {_id: req.body.id};
                const newValues = {
                    $set: {
                        name: req.body.data.name,
                        brand: req.body.data.brand,
                        model: req.body.data.model,
                        lat: req.body.data.lat,
                        long: req.body.data.long,
                        ip: req.body.data.ip,
                        type: req.body.data.type,
                    }
                };
                mina.findOneAndUpdate(myQuery, newValues, (err, doc) => {
                    if (err) res.status(400).json({message: 'Error al editar el dispositivo de Mina'});
                    if (!doc) res.status(400).json({message: 'Error al editar el dispositivo de Mina'});
                    else{
                        ctrlLog.saveLog(doc.Email,"Edición de dispositivo de Mina");
                        res.status(200).json({message: 'Procedimiento correcto'});
                    }
                });
            }
            else res.status(400).json({message:'Error al editar dispositivo de Mina'});
        }
    )
};

let create = (req, res ) => {
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.mina && user.roles.devices){
                let device = new mina({
                    name: req.body.name,
                    brand: req.body.brand,
                    model: req.body.model,
                    lat: req.body.lat,
                    long: req.body.long,
                    ip: req.body.ip,
                    type: req.body.type,
                    state: false
                });
                device.save((err) => {
                    if (err) res.status(400).json({message:'Error al crear dispositivo de Mina'});
                    else{
                        ctrlLog.saveLog(user.Email,"Creacion de dispositivo de Mina");
                        res.status(200).json({message:'Procedimiento correcto'});
                    }
                });
            }
            else res.status(400).json({message:'Error al editar dispositivo de Mina'});
        }
    );
};

let deleteDevice = (req, res ) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.lora && user.roles.devices){
                const myQuery = {_id: req.params.id};
                const newValues = {
                    $set: {
                        deleted: true,
                    }
                };
                mina.findOneAndUpdate(myQuery, newValues, (err, ) => {
                    // As always, handle any potential errors:
                    if (err) return res.status(500).send(err);
                    ctrlLog.saveLog(user.Email,"Eliminación de dispositivo Lora");
                    res.status(200).json({message: 'Procedimiento correcto'});
                });
            }
            else res.status(400).json({message:'Error al obtener usuarios'});
        }
    )
};

let maps = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.mina) {
                sistema.find({}).then((systems) => {
                    let urlRadio = systems.filter((key) => key.name === 'Radio')[0].url;
                    let urlSwitch = systems.filter((key) => key.name === 'Switch')[0].url;
                    let urlCamara = systems.filter((key) => key.name === 'Cámara')[0].url;
                    let urlNvr = systems.filter((key) => key.name === 'NVR')[0].url;
                    let urlGateway = systems.filter((key) => key.name === 'Gateway')[0].url;
                    mina.find({}).then((devices)=>{
                        let final=[];
                        devices.forEach((device)=>{
                            let url = '';
                            let fecha = '';
                            switch (device.type) {
                                case 'Radio':
                                    url = urlRadio;
                                    break;
                                case 'Switch':
                                    url = urlSwitch;
                                    break;
                                case 'Cámara':
                                    url = urlCamara;
                                    break;
                                case 'NVR':
                                    url = urlNvr;
                                    break;
                                case 'Gateway':
                                    url = urlGateway;
                                    break;
                                default:
                                    break;
                            }
                            if(device.state) url=`${url}_on.svg`;
                            else url=`${url}_off.svg`;
                            if (device.last){
                                let date1 = new Date(device.last);
                                fecha = `<h5><strong>Ultima conexión:</strong> ${date1.toLocaleString('en-GB')}</h5>`;
                            }
                            let popOver ={
                                name:device.name,
                                lat:device.lat,
                                long:device.long,
                                message:`<h4><strong>${device.name}</strong></h4><h5><strong>Dispositivo:</strong> ${device.type}</h5>${fecha}<h5><strong>Marca:</strong>${device.brand}</h5><h5><strong>Modelo:</strong>${device.model}</h5><h5><strong>Url:</strong><a href=http://${device.ip} target=_blank>${device.ip}</a></h5>`,
                                type:device.type,
                                state:device.state,
                                key:'mina',
                                url: url,
                                id: device._id
                            };
                            final.push(popOver)
                        });
                        res.send(final);
                    });
                });
            }
            else res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
        }
    )
};


module.exports = {
    devices,
    edit,
    create,
    deleteDevice,
    maps
};

