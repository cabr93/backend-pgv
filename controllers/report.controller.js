const mongoose = require('mongoose');
const User = mongoose.model('User');
let pdf = require('html-pdf');
const radio = mongoose.model('radio');
const radiosInf = mongoose.model('radiosInf');
const sendMail = require('../email/emailHandler');
const lora = mongoose.model('lora');
const loraInf = mongoose.model('loraInf');
const peaje = mongoose.model('peaje');
const peajesInf = mongoose.model('peajesInf');
const mina = mongoose.model('mina');
const minaInf = mongoose.model('minaInf');

let reports = (req, res) => {
    User.findById({ _id: req._id },
        (err, user) => {
            if (err) res.status(400).json({ message: 'Error al encontrar usuario' });
            else if (!user) res.status(400).json({ message: 'Error al encontrar usuario' });
            else if (user.roles.reports) {
                let start = new Date(req.body.dates[0]).toLocaleDateString("ko-KR", { timeZone: "America/New_York" });
                let end = new Date(req.body.dates[1]).toLocaleDateString("ko-KR", { timeZone: "America/New_York" });
                let current = new Date(req.body.dates[0]);
                let devices = req.body.systems;
                for (let i = 0; i < devices.length; i++) {
                    switch (devices[i]) {
                        case 'radios':
                            setTimeout(()=> {
                                radioReport(start, end, current, user);
                            }, 0);
                            break;
                        case 'lora':
                            setTimeout(()=> {
                                loraReport(start, end, current, user);
                            }, 1000);
                            break;
                        case 'tolls':
                            setTimeout(()=> {
                                tollReport(start, end, current, user);
                            }, 2000);
                            break;
                        case 'mina':
                            setTimeout(()=> {
                                minaReport(start, end, current, user);
                            }, 3000);
                            break;
                        default:
                            break;
                    }
                }
                setTimeout(() => {
                    res.status(200).json({ message: 'Procedimiento correcto' });
                }, 2000);
            }
            else res.status(400).json({ message: 'Error al crear reportes' });
        }
    )
};


// Radio report
let resolveQueryRadio = (day) => {
    return new Promise((resolve, reject) => {
        radiosInf.find({ day }, { day: 0, _id: 0 },
            (err, data) => {
                if (err) reject('error');
                else if (Array.isArray(data) && data.length === 0) reject('sin data');
                else resolve(data[0]._doc)
            }
        );
    })
};
let radioReport = async(inicio, fin, actual, user) => {
    let actual2 = inicio;
    let data = {};
    let datat = {};
    let dataf = {};
    await resolveQueryRadio(actual2).then(result => data = result, err => err)
    if (Object.keys(data).length !== 0) {
        let ok = 0;
        let error = 0;
        let keys = Object.keys(data);
        datat = data;
        keys.forEach(element => {
            ok += data[element].ok;
            error += data[element].error;
        });
        dataf[actual2] = { ok, error };
    }
    while (fin !== actual2) {
        actual = new Date(actual.setDate(actual.getDate() + 1))
        actual2 = new Date(actual.setDate(actual.getDate())).toLocaleDateString("ko-KR", { timeZone: "America/New_York" })
        let data = {};
        await resolveQueryRadio(actual2).then(result => data = result, err => err)
        if (Object.keys(data).length !== 0) {
            if (Object.keys(datat).length === 0) {
                let ok = 0;
                let error = 0;
                let keys = Object.keys(data);
                datat = data;
                keys.forEach(element => {
                    ok += data[element].ok;
                    error += data[element].error;
                });
                dataf[actual2] = { ok, error };
            } else {
                let keys = Object.keys(data);
                let ok = 0;
                let error = 0;
                keys.forEach(element => {
                    ok += data[element].ok;
                    error += data[element].error;
                    try {
                        datat[element].ok += data[element].ok;
                        datat[element].error += data[element].error;
                    } catch (error) {
                        datat[element] = data[element];
                    }
                });
                dataf[actual2] = { ok, error };
            }
        }
    }
    let datafinal = {};
    let fechasFinal = { max: { cumplido: 0, id: '', ok: 0, error: 0 }, min: { cumplido: 150, id: '', ok: 0, error: 0 } };
    radio.find({}).then((todos) => {
        let dispositivo = 0
        Object.keys(datat).map(key => {
            dispositivo += 1;
            let aa = todos.find(data => String(data._id) === String(key));
            let cumplido = datat[aa._id].ok / (datat[aa._id].ok + datat[aa._id].error) * 100
            cumplido = cumplido.toFixed(1)
            if (datat[aa._id].ok === 0 && datat[aa._id].error === 0)
                cumplido = 0
            datafinal[aa._id] = { name: aa.name, system: aa.system, ok: datat[aa._id].ok, error: datat[aa._id].error, cumplido }
        });
        let okf = 0;
        let errorf = 0;
        Object.keys(dataf).map(key => {
            let cumplido = (dataf[key].ok / (dataf[key].ok + dataf[key].error)) * 100
            cumplido = cumplido.toFixed(1)
            if (dataf[key].ok === 0 && dataf[key].error === 0)
                cumplido = 0
            if (cumplido >= fechasFinal.max.cumplido) {
                fechasFinal.max = { id: key, ok: dataf[key].ok, error: dataf[key].error, cumplido }
            }
            if (cumplido <= fechasFinal.min.cumplido) {
                fechasFinal.min = { id: key, ok: dataf[key].ok, error: dataf[key].error, cumplido }
            }
            okf += dataf[key].ok
            errorf += dataf[key].error
        });
        let cumplifof = okf / (errorf + okf);
        if (okf === 0 && errorf === 0)
            cumplifof = 0;
        cumplifof = cumplifof * 100;
        cumplifof = cumplifof.toFixed(1);
        endReport(inicio, fin, datafinal, fechasFinal, cumplifof, 'radios', dispositivo, user)
    });
};

// LoRa report
let resolveQueryLora = (fecha) => {
    return new Promise((resolve, reject) => {
        loraInf.find({ day: fecha }, { day: 0, _id: 0 },
            (err, data) => {
                if (err)
                    reject('error')
                else if (Array.isArray(data) && data.length === 0) {
                    reject('sin data')
                } else {
                    resolve(data[0]._doc)
                }
            }
        );
    })
};
let loraReport = async(inicio, fin, actual, user) => {
    let actual2 = inicio;
    let data = {};
    let datat = {};
    let dataf = {};
    await resolveQueryLora(actual2).then(result => data = result, err => err);
    if (Object.keys(data).length !== 0) {
        let ok = 0;
        let error = 0;
        let keys = Object.keys(data);
        datat = data;
        keys.forEach(element => {
            ok += data[element].ok;
            error += data[element].error;
        });
        dataf[actual2] = { ok, error };
    }
    while (fin !== actual2) {
        actual = new Date(actual.setDate(actual.getDate() + 1))
        actual2 = new Date(actual.setDate(actual.getDate())).toLocaleDateString("ko-KR", { timeZone: "America/New_York" })
        let data = {};
        await resolveQueryLora(actual2).then(result => data = result, err => err);
        if (Object.keys(data).length !== 0) {
            if (Object.keys(datat).length === 0) {
                let ok = 0;
                let error = 0;
                let keys = Object.keys(data);
                datat = data;
                keys.forEach(element => {
                    ok += data[element].ok;
                    error += data[element].error;
                });
                dataf[actual2] = { ok, error };
            } else {
                let keys = Object.keys(data);
                let ok = 0;
                let error = 0;
                keys.forEach(element => {
                    ok += data[element].ok;
                    error += data[element].error;
                    try {
                        datat[element].ok += data[element].ok;
                        datat[element].error += data[element].error;
                    } catch (error) {
                        datat[element] = data[element];
                    }
                });
                dataf[actual2] = { ok, error };
            }
        }
    }
    let datafinal = {};
    let fechasFinal = { max: { cumplido: 0, id: '', ok: 0, error: 0 }, min: { cumplido: 150, id: '', ok: 0, error: 0 } };
    lora.find({}).then((todos) => {
        let dispositivo = 0
        Object.keys(datat).map(key => {
            dispositivo += 1;
            let aa = todos.find(data => String(data._id) === String(key));
            let cumplido = datat[aa._id].ok / (datat[aa._id].ok + datat[aa._id].error) * 100;
            cumplido = cumplido.toFixed(1);
            if (datat[aa._id].ok === 0 && datat[aa._id].error === 0)
                cumplido = 0;
            datafinal[aa._id] = { name: aa.name, system: 'lora', ok: datat[aa._id].ok, error: datat[aa._id].error, cumplido }
        });
        let okf = 0;
        let errorf = 0;
        Object.keys(dataf).map(key => {
            let cumplido = (dataf[key].ok / (dataf[key].ok + dataf[key].error)) * 100
            cumplido = cumplido.toFixed(1)
            if (dataf[key].ok === 0 && dataf[key].error === 0)
                cumplido = 0
            if (cumplido >= fechasFinal.max.cumplido) {
                fechasFinal.max = { id: key, ok: dataf[key].ok, error: dataf[key].error, cumplido }
            }
            if (cumplido <= fechasFinal.min.cumplido) {
                fechasFinal.min = { id: key, ok: dataf[key].ok, error: dataf[key].error, cumplido }
            }
            okf += dataf[key].ok;
            errorf += dataf[key].error;
        });
        let cumplifof = okf / (errorf + okf)
        if (okf === 0 && errorf === 0)
            cumplifof = 0;
        cumplifof = cumplifof * 100;
        cumplifof = cumplifof.toFixed(1);
        endReport(inicio, fin, datafinal, fechasFinal, cumplifof, 'lora', dispositivo, user)
    });

};

// Tolls report
let resolveQueryToll = (fecha) => {
    return new Promise((resolve, reject) => {
        peajesInf.find({ day: fecha }, { day: 0, _id: 0 },
            (err, data) => {
                if (err)
                    reject('error');
                else if (Array.isArray(data) && data.length === 0) {
                    reject('sin data');
                } else {
                    resolve(data[0]._doc);
                }
            }
        );
    })
};
let tollReport = async(inicio, fin, actual, user) => {
    let actual2 = inicio;
    let data = {};
    let datat = {};
    let dataf = {};
    await resolveQueryToll(actual2).then(result => data = result, err => err)
    if (Object.keys(data).length !== 0) {
        let ok = 0;
        let error = 0;
        let keys = Object.keys(data);
        datat = data;
        keys.forEach(element => {
            ok += data[element].ok;
            error += data[element].error;
        });
        dataf[actual2] = { ok, error };
    }
    while (fin !== actual2) {
        actual = new Date(actual.setDate(actual.getDate() + 1));
        actual2 = new Date(actual.setDate(actual.getDate())).toLocaleDateString("ko-KR", { timeZone: "America/New_York" })
        let data = {};
        await resolveQueryToll(actual2).then(result => data = result, err => err)
        if (Object.keys(data).length !== 0) {
            if (Object.keys(datat).length === 0) {
                let ok = 0;
                let error = 0;
                let keys = Object.keys(data);
                datat = data;
                keys.forEach(element => {
                    ok += data[element].ok;
                    error += data[element].error;
                });
                dataf[actual2] = { ok, error };
            } else {
                let keys = Object.keys(data);
                let ok = 0;
                let error = 0;
                keys.forEach(element => {
                    ok += data[element].ok;
                    error += data[element].error;
                    try {
                        datat[element].ok += data[element].ok;
                        datat[element].error += data[element].error;
                    } catch (error) {
                        datat[element] = data[element];
                    }
                });
                dataf[actual2] = { ok, error };
            }
        }
    }
    let datafinal = {};
    let fechasFinal = { max: { cumplido: 0, id: '', ok: 0, error: 0 }, min: { cumplido: 150, id: '', ok: 0, error: 0 } };
    peaje.find({}).then((todos) => {
        let dispositivo = 0
        Object.keys(datat).map(key => {
            dispositivo += 1;
            let dradios = todos.find(data => String(data._id+'ip') === String(key));
            let dswitch = todos.find(data => String(data._id+'ipSw') === String(key));
            let dnvr = todos.find(data => String(data._id+'ipNvr') === String(key));
            let dgateway = todos.find(data => String(data._id+'ipGateway') === String(key));
            let dcamara = todos.find(data => String(data._id+'ipCam') === String(key));

            try {
                let cumplido = datat[dradios._id+'ip'].ok / (datat[dradios._id+'ip'].ok + datat[dradios._id+'ip'].error) * 100;
                cumplido = Math.floor(cumplido*10)/10
                cumplido = cumplido.toFixed(1);
                if (datat[dradios._id+'ip'].ok === 0 && datat[dradios._id+'ip'].error === 0)
                    cumplido = 0;
                datafinal[dradios._id+'ip'] = { name: dradios.name, system: 'radios', ok: datat[dradios._id+'ip'].ok, error: datat[dradios._id+'ip'].error, cumplido }
            } catch (error) { error }

            try {
                let cumplido = datat[dswitch._id+'ipSw'].ok / (datat[dswitch._id+'ipSw'].ok + datat[dswitch._id+'ipSw'].error) * 100;
                cumplido = Math.floor(cumplido*10)/10
                cumplido = cumplido.toFixed(1);
                if (datat[dswitch._id+'ipSw'].ok === 0 && datat[dswitch._id+'ipSw'].error === 0)
                    cumplido = 0;
                datafinal[dswitch._id+'ipSw'] = { name: dswitch.name, system: 'Switch', ok: datat[dswitch._id+'ipSw'].ok, error: datat[dswitch._id+'ipSw'].error, cumplido }
            } catch (error) { error }

            try {
                let cumplido = datat[dnvr._id+'ipNvr'].ok / (datat[dnvr._id+'ipNvr'].ok + datat[dnvr._id+'ipNvr'].error) * 100;
                cumplido = Math.floor(cumplido*10)/10
                cumplido = cumplido.toFixed(1);
                if (datat[dnvr._id+'ipNvr'].ok === 0 && datat[dnvr._id+'ipNvr'].error === 0)
                    cumplido = 0;
                datafinal[dnvr._id+'ipNvr'] = { name: dnvr.name, system: 'NVR', ok: datat[dnvr._id+'ipNvr'].ok, error: datat[dnvr._id+'ipNvr'].error, cumplido }
            } catch (error) { error }

            try {
                let cumplido = datat[dgateway._id+'ipGateway'].ok / (datat[dgateway._id+'ipGateway'].ok + datat[dgateway._id+'ipGateway'].error) * 100;
                cumplido = Math.floor(cumplido*10)/10
                cumplido = cumplido.toFixed(1);
                if (datat[dgateway._id+'ipGateway'].ok === 0 && datat[dgateway._id+'ipGateway'].error === 0)
                    cumplido = 0;
                datafinal[dgateway._id+'ipGateway'] = { name: dgateway.name, system: 'Gateway', ok: datat[dgateway._id+'ipGateway'].ok, error: datat[dgateway._id+'ipGateway'].error, cumplido }
            } catch (error) { error }

            try {
                let cumplido = datat[dcamara._id+'ipCam'].ok / (datat[dcamara._id+'ipCam'].ok + datat[dcamara._id+'ipCam'].error) * 100;
                cumplido = Math.floor(cumplido*10)/10
                cumplido = cumplido.toFixed(1);
                if (datat[dcamara._id+'ipCam'].ok === 0 && datat[dcamara._id+'ipCam'].error === 0)
                    cumplido = 0;
                datafinal[dcamara._id+'ipCam'] = { name: dcamara.name, system: 'Cámara', ok: datat[dcamara._id+'ipCam'].ok, error: datat[dcamara._id+'ipCam'].error, cumplido }
            } catch (error) { error }

        });
        let okf = 0;
        let errorf = 0;
        Object.keys(dataf).map(key => {
            let cumplido = (dataf[key].ok / (dataf[key].ok + dataf[key].error)) * 100
            cumplido = cumplido.toFixed(1)
            if (dataf[key].ok === 0 && dataf[key].error === 0)
                cumplido = 0
            if (cumplido >= fechasFinal.max.cumplido) {
                fechasFinal.max = { id: key, ok: dataf[key].ok, error: dataf[key].error, cumplido }
            }
            if (cumplido <= fechasFinal.min.cumplido) {
                fechasFinal.min = { id: key, ok: dataf[key].ok, error: dataf[key].error, cumplido }
            }
            okf += dataf[key].ok
            errorf += dataf[key].error
        });
        let cumplifof = okf / (errorf + okf)
        if (okf === 0 && errorf === 0)
            cumplifof = 0
        // cumplifof = Math.round(cumplifof * 1000) / 10;
        cumplifof = cumplifof * 100
        cumplifof = cumplifof.toFixed(1)
        endReport(inicio, fin, datafinal, fechasFinal, cumplifof, 'peaje', dispositivo, user)
    });
};

// Mina report
let resolveQueryMina = (fecha) => {
    return new Promise((resolve, reject) => {
        minaInf.find({ day: fecha }, { day: 0, _id: 0 },
            (err, data) => {
                if (err)
                    reject('error')
                else if (Array.isArray(data) && data.length === 0) {
                    reject('sin data')
                } else {
                    resolve(data[0]._doc)
                }
            }
        );
    })
};
let minaReport = async(inicio, fin, actual, user) => {
    let actual2 = inicio
    let data = {};
    let datat = {};
    let dataf = {};
    await resolveQueryMina(actual2).then(result => data = result, err => err)
    if (Object.keys(data).length !== 0) {
        let ok = 0;
        let error = 0;
        let keys = Object.keys(data);
        datat = data;
        keys.forEach(element => {
            ok += data[element].ok;
            error += data[element].error;
        });
        dataf[actual2] = { ok, error };
    }
    while (fin !== actual2) {
        actual = new Date(actual.setDate(actual.getDate() + 1))
        actual2 = new Date(actual.setDate(actual.getDate())).toLocaleDateString("ko-KR", { timeZone: "America/New_York" })
        let data = {};
        await resolveQueryMina(actual2).then(result => data = result, err => err)
        if (Object.keys(data).length !== 0) {
            if (Object.keys(datat).length === 0) {
                let ok = 0;
                let error = 0;
                let keys = Object.keys(data);
                datat = data;
                keys.forEach(element => {
                    ok += data[element].ok;
                    error += data[element].error;
                });
                dataf[actual2] = { ok, error };
            } else {
                let keys = Object.keys(data);
                let ok = 0;
                let error = 0;
                keys.forEach(element => {
                    ok += data[element].ok;
                    error += data[element].error;
                    try {
                        datat[element].ok += data[element].ok;
                        datat[element].error += data[element].error;
                    } catch (error) {
                        datat[element] = data[element];
                    }
                });
                dataf[actual2] = { ok, error };
            }
        }
    }
    let datafinal = {};
    let fechasFinal = { max: { cumplido: 0, id: '', ok: 0, error: 0 }, min: { cumplido: 150, id: '', ok: 0, error: 0 } };
    mina.find({}).then((todos) => {
        let dispositivo = 0
        Object.keys(datat).map(key => {
            dispositivo += 1;
            let aa = todos.find(data => String(data._id) === String(key));
            let cumplido = datat[aa._id].ok / (datat[aa._id].ok + datat[aa._id].error) * 100
            cumplido = cumplido.toFixed(1)
            if (datat[aa._id].ok === 0 && datat[aa._id].error === 0)
                cumplido = 0
            datafinal[aa._id] = { name: aa.name, system: aa.type, ok: datat[aa._id].ok, error: datat[aa._id].error, cumplido }
        })
        let okf = 0;
        let errorf = 0;
        Object.keys(dataf).map(key => {
            let cumplido = (dataf[key].ok / (dataf[key].ok + dataf[key].error)) * 100
            cumplido = cumplido.toFixed(1)
            if (dataf[key].ok === 0 && dataf[key].error === 0)
                cumplido = 0
            if (cumplido >= fechasFinal.max.cumplido) {
                fechasFinal.max = { id: key, ok: dataf[key].ok, error: dataf[key].error, cumplido }
            }
            if (cumplido <= fechasFinal.min.cumplido) {
                fechasFinal.min = { id: key, ok: dataf[key].ok, error: dataf[key].error, cumplido }
            }
            okf += dataf[key].ok
            errorf += dataf[key].error
        })
        let cumplifof = okf / (errorf + okf)
        if (okf === 0 && errorf === 0)
            cumplifof = 0
        // cumplifof = Math.round(cumplifof * 1000) / 10;
        cumplifof = cumplifof * 100
        cumplifof = cumplifof.toFixed(1)
        endReport(inicio, fin, datafinal, fechasFinal, cumplifof, 'mina', dispositivo, user)
    });

};

// Report
let endReport = async (inicio, fin, data, data2, cumplido, tipo, dispositivo, user) => {
    var contenido = `
    <html>
        <head>
            <meta charset="utf8">
            <title>Informe</title>
            <style>
                .column {
                    float: left;
                    width: 50%;
                }
                .row:after {
                    content: "";
                    display: table;
                    clear: both;
                }
                .logo {
                    position: relative;
                    width: 30%;
                    left: 60%;
                    top: 30%;
                }
                .coltexti {
                    color: #1a237e;
                    font-family: 'Franklin Gothic Medium';
                }
                .coltextc {
                    color: #1a237e;
                    font-family: 'calibri';
                }
                .coltextc2 {
                    color: #9d9d9c;
                    font-family: 'calibri';
                }
                td:first-child {
                    border-right: 2px solid #1a237e;
                }
                div.middle-div {
                    display: -webkit-flex;
                    display: flex;
                    align-items: center;
                    height: 120px;
                }
                .bordes {
                    margin: 5%;
                    margin-top: 2%;
                    margin-bottom: 2%;
                }
                tr.border_bottom td {
                    border-bottom: 1pt solid black;
                }
                .circulo {
                    width: 1rem;
                    height: 1rem;
                    border-radius: 50%;
                    background: red;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    text-align: center;
                    margin: 0px auto;
                    padding: 3%
                }
                #circulo {
                    height: 50px;
                    width: 50px;
                    text-align: center;
                    font-size: 40px;
                    vertical-align: middle;
                    border-radius: 50%;
                    background: white;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    text-align: center;
                    margin: 0px auto;
                    border: solid 2px #b2b2b2;
                    color: #b2b2b2;
                }
            </style>
        </head>
        <div id="pageHeader" style="line-height : 30px">
            <div style=" margin:0px;" class="row">
                <div class="column">
                    <h1 class="coltexti"> INFORME DE GESTI&Oacute;N POR SISTEMA </h1>
                    <h2 class="coltextc">SIMS TECHNOLOGIES SAS</h2>
                </div>
                <div class="column">
                    <img style="height: 100px; width: 140px;" class="logo" src="https://gruposims.com.co/wp-content/uploads/2017/11/home_SIMS_final-04-300x213.png" />
                </div>
            </div>
            <table align="center" style="width: 100%; border-radius: 15px; border: 2px solid #1a237e">
                <tbody>
                    <tr>
                        <td style="width: 50%;">
                            <h3 class="coltextc" style="margin: 1%;margin-left: 5%;">Fecha Inicio <span style="text-align: center;" class="coltextc2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${inicio}</span> </h3>
                        </td>
                        <td style="width: 50%;">
                            <h3 class="coltextc" style="margin: 1%;margin-left: 5%;">Fecha final <span class="coltextc2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${fin}</span> </h3>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="pageFooter" style "margin:0%;margin-top:5%">
            <div id="circulo" style "margin-top:5%;font-family: 'Arial Narrow'">
                {{page}}
            </div>
        </div>
        <!-- body  -->
        <div style="margin:0px;line-height : 15px">
            <div class="row" style="background-color: #f6f6f6;">
                <div class="column middle-div" style="margin-left:3%;;width: 32%">
                    <div>
                        <h3 class="coltexti"> Sistema de ${tipo}</h3>
                        <h4 class="coltextc2">Durante el periodo selecionado el sistema de ${tipo} conformado por ${dispositivo} dispositivos obtuvo un:</h4>
                    </div>
                </div>
                <div class="column" style="margin-left:2%;margin-right:2%;width: 59%">
                    <div class="row">
                        <div class="column middle-div" style="width: 50%">
                            <h1 style="font-size:450%;text-align: center; font-family: 'Arial Narrow';" class="coltexti"> ${cumplido}% </h1>
                        </div>
                        <div class="column middle-div" style="width: 45%;line-height : 25px;margin-left:3%;">
                            <h2 style="text-align: left" class="coltexti">PORCENTAJE DE FUNCIONAMIENTO</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr style="margin: 0px;background-color: #f6f6f6;margin-top: 2%;margin-bottom: 1%">
        <h4 class="coltextc2" style="text-align: center;"> Compracion de los niveles de sevicios entre el dia de mejor prestacion y el dia de mayores caidas</h4>
        <table align="center" style=" width: 100%; border-radius: 15px; border: 2px solid #b2b2b2;">
            <tbody>
                <tr>
                    <td style="width: 33.3%;border-right: 2px solid #b2b2b2;">
                        <h4 class="coltextc2" style="margin: 1%;margin-left: 5%;">Día con
                            <span style="color: #575756;">menor</span> porcentaje de funcionalidad del servicio.</h4>
                    </td>
                    <td style="width: 33.3%;border-right: 2px solid #b2b2b2;">
                        <h3 class="coltextc2" style="text-align: center; font-size: 31px; margin: 1%;margin-left: 5%;">${data2.min.id}</h3>
                    </td>
                    <td style="width: 33.3%;">
                        <h3 class="coltextc2" style="text-align: center; font-size: 31px; margin: 1%;margin-left: 5%;">${data2.min.cumplido}% </h3>
                    </td>
                </tr>
            </tbody>
        </table>
        <table align="center" style="margin-bottom:2%; width: 100%; border-radius: 15px; border: 2px solid #b2b2b2;margin-top: 2px ;">
            <tbody>
                <tr>
                    <td style="width: 33.3%;border-right: 2px solid #b2b2b2;">
                        <h4 class="coltextc2" style="margin: 1%;margin-left: 5%;">Día con
                            <span style="color: #575756;">mayor</span> porcentaje de funcionalidad del servicio.</h4>
                    </td>
                    <td style="width: 33.3%;border-right: 2px solid #b2b2b2;">
                        <h3 class="coltextc2" style="text-align: center; font-size: 31px; margin: 1%;margin-left: 5%;">${data2.max.id} </h3>
                    </td>
                    <td style="width: 33.3%;">
                        <h3 class="coltextc2" style="text-align: center; font-size: 31px; margin: 1%;margin-left: 5%;"> ${data2.max.cumplido}% </h3>
                    </td>
                </tr>
            </tbody>
        </table>
        <div style="margin-top:5%;margin:1%;margin-bottom:3%" class="row">
            <div class="column">
                <h4 style="margin: 0%;" class="coltextc">Detalle de funcionalidad por dispositivo.</h4>
            </div>
            <div class="column">
                <table align="right" style=" width: 60%;background-color: #1a237e;">
                    <tbody>
                        <tr>
                            <td style="width: 50%;border: 2px solid #1a237e; background-color: #1a237e; ">
                                <h5 class="coltextc " style="color: #ffffff; margin: 1%;margin-left: 5%; ">Pings ideales</h5>
                            </td>
                            <td style="width: 50%;border: 2px solid #1a237e;background-color: #ffffff; ">
                                <h5 class="coltextc " style="margin: 1%;margin-left: 5%; "> </h5>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <table align="center" style="width: 100%; border-collapse: collapse">
                <tbody>
                    <tr style="background-color:#f2f2f2;height: 50px;">
                        <th class="coltextc">Nombre</th>
                        <th class="coltextc">Tipo</th>
                        <th class="coltextc">Cant. Ping exito</th>
                        <th class="coltextc">Cant. Ping Perdidos</th>
                        <th class="coltextc">% cumplimiento</th>
                    </tr>`
                    Object.keys(data).map(key => {
                        contenido = contenido + `
                        <tr style="height: 50px;border-bottom: 1px solid #1a237e">
                            <th class="coltextc">${data[key].name}</th>
                            <th class="coltextc">${data[key].system}</th>
                            <th class="coltextc">${data[key].ok}</th>
                            <th class="coltextc">${data[key].error}</th>
                            <th class="coltextc">${data[key].cumplido}</th>
                        </tr>`
                    })
                    contenido = contenido + `
                </tbody>
            </table>
        </div>
    </html>`;
    if (dispositivo===0) {
        contenido=`
        <html>
            <head>
                <meta charset="utf8">
                <title>Informe</title>
                <style>
                    .column {
                        float: left;
                        width: 50%;
                    }
                    
                    .row:after {
                        content: "";
                        display: table;
                        clear: both;
                    }
                    
                    .logo {
                        position: relative;
                        width: 30%;
                        left: 60%;
                        top: 30%;
                    }
                    
                    .coltexti {
                        color: #1a237e;
                        font-family: 'Franklin Gothic Medium';
                    }
                    
                    .coltextc {
                        color: #1a237e;
                        font-family: 'calibri';
                    }
                    
                    .coltextc2 {
                        color: #9d9d9c;
                        font-family: 'calibri';
                    }
                    
                    td:first-child {
                        border-right: 2px solid #1a237e;
                    }
                    
                    div.middle-div {
                        display: -webkit-flex;
                        display: flex;
                        align-items: center;
                        height: 120px;
                    }
                    
                    .bordes {
                        margin: 5%;
                        margin-top: 2%;
                        margin-bottom: 2%;
                    }
                    
                    tr.border_bottom td {
                        border-bottom: 1pt solid black;
                    }
                    
                    .circulo {
                        width: 1rem;
                        height: 1rem;
                        border-radius: 50%;
                        background: red;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        text-align: center;
                        margin: 0px auto;
                        padding: 3%
                    }
                    
                    #circulo {
                        height: 50px;
                        width: 50px;
                        text-align: center;
                        font-size: 40px;
                        vertical-align: middle;
                        border-radius: 50%;
                        background: white;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        text-align: center;
                        margin: 0px auto;
                        border: solid 2px #b2b2b2;
                        color: #b2b2b2;
                    }
                </style>
            </head>
            <div id="pageHeader" style="line-height : 30px">
                <div style=" margin:0%;" class="row">
                    <div class="column">
                        <h1 class="coltexti"> INFORME DE GESTI&Oacute;N POR SISTEMA </h1>
                        <h2 class="coltextc">SIMS TECHNOLOGIES SAS</h2>
                    </div>
                    <div class="column">
                        <img style="height: 100px; width: 140px;" class="logo" src="https://gruposims.com.co/wp-content/uploads/2017/11/home_SIMS_final-04-300x213.png" />
                    </div>
                </div>
                <table align="center" style="width: 100%; border-radius: 15px; border: 2px solid #1a237e">
                    <tbody>
                        <tr>
                            <td style="width: 50%;">
                                <h3 class="coltextc" style="margin: 1%;margin-left: 5%;">Fecha Inicio <span style="text-align: center;" class="coltextc2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${inicio}</span> </h3>
                            </td>
                            <td style="width: 50%;">
                                <h3 class="coltextc" style="margin: 1%;margin-left: 5%;">Fecha final <span class="coltextc2"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${fin}</span> </h3>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="pageFooter" style "margin:0%;margin-top:5%">
                <div id="circulo" style "margin-top:5%;font-family: 'Arial Narrow'">
                    {{page}}
                </div>
            </div>
            <!-- body  -->
            <div style="margin:0%;line-height : 25px">
                <div class="row" style="background-color: #f6f6f6;margin:10px">
                    <div style="width: 100%;text-align: center;margin:10px">
                    <br>
                            <h3 class="coltexti" > El sistema de ${tipo} durante el periodo seleccionado <br>  no tuvo ningun registro de eventos</h3>
                            <br>
                    </div>
                </div>
            </div>
        </html>`}
    const config = {
        "format": "Letter", // allowed units: A3, A4, A5, Legal, Letter, Tabloid
        "orientation": "portrait", // portrait or landscape
        // Page options
        "border": {
            "top": "0cm", // default is 0, units: mm, cm, in, px
            "right": "1cm",
            "bottom": "0cm",
            "left": "1cm"
        },
        paginationOffset: 2, // Override the initial pagination number
        "header": {
            "height": "230px",
            "contents": ``
        },
        "footer": {
            "height": "110px"
        },
        // Zooming option, can be used to scale images if `options.type` is not pdf
        "zoomFactor": "1", // default is 1
        // File options
        "type": "pdf", // allowed file types: png, jpeg, pdf
        "quality": "75", // only used for types png & jpeg
    };
    let prefijo = generateId();
    pdf.create(contenido, config).toFile(`./salida${tipo}${prefijo}.pdf`, function(err, res) {
        if (err)  console.log(err);
        else sendMail.reportEmail(user, tipo, prefijo);
    });
};
let generateId = () =>{
    let length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (let i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
};

module.exports = {
    reports
};
