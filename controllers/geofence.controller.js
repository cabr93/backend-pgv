const mongoose = require('mongoose');

const User = mongoose.model('User');

const geofence = mongoose.model('geocercas');
const Rastreo = mongoose.model('rastreo');


let getGeofence = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err)
                res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user)
                res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.roles.geofence) {
                geofence.find({}).then((todos)=>{
                    Rastreo.find({}).then((dispositivos)=>{
                        todos.map( (geo,idx) =>{
                            geo.devices.forEach((element, index) => {
                                todos[idx].devices[index]={ id:element, name:dispositivos.filter(key => String(key._id) === String(element))[0].placa}
                            });
                            // dispositivos.filter(key => key._id===)
                        })
                        res.send(todos);
                    })
                });
            }
            else{
                res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
            }
        }
    )
}

let geofenceAdd = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err)
                res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user)
                res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.roles.geofence) {
                let geo = new geofence({
                    name: req.body.name,
                    devices: req.body.devices,
                    type: req.body.tipo,
                    coords: req.body.coords
                });
                geo.save((err, doc) => {
                    if (err)
                        res.status(400).json({message:'Error al agregar geocerca'});
                    else{
                        res.status(200).json({message:'Procedimiento correcto'});
                    }
                });
            }
            else{
                res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
            }
        }
    )
}

let geofenceEdit = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err)
                res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user)
                res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.roles.geofence) {
                geofence.findById({_id:req.body.id},
                    (err, geo)=>{
                        if (err)
                            //error en la opeacion de cambio de contrseña
                            res.status(400).json({message:'Error al cambiar la clave'});
                        else if (!geo)
                            res.status(400).json({message:'Error al encontrar ususario'});
                        else{
                            let myquery = { _id:req.body.id};
                            let newvalues = { $set: {
                                    name: req.body.name,
                                    devices: req.body.devices,
                                    type: req.body.tipo,
                                    coords: req.body.coords
                                }};
                            geofence.findOneAndUpdate(myquery, newvalues, (err,doc)=>{
                                if (err)
                                    res.status(400).json({message:'Error al editar dispositivo LoRa'});
                                else{
                                    res.status(200).json({message:'Procedimiento correcto'});
                                }
                            })
                        }
                    }
                )
            }
            else{
                res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
            }
        }
    )
}

let deleteGeofence = (req, res, next) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err)
                res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user)
                res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.lora && user.roles.devices){
                geofence.findOneAndRemove({_id: req.params.id}, (err, userd) => {
                    // As always, handle any potential errors:
                    if (err) return res.status(500).send(err);
                    return res.status(200).json({message:'Procedimiento correcto'});
                });
            }
            else{
                res.status(400).json({message:'Error al obtener usuarios'});
            }
        }
    )
}

module.exports = {
    getGeofence,
    geofenceAdd,
    geofenceEdit,
    deleteGeofence
};