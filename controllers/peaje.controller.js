const mongoose = require('mongoose');
const peaje = mongoose.model('peaje');
const User = mongoose.model('User');
const ctrlLog  = require('../controllers/log.controller');
const sistema = mongoose.model('sistema');

let devices = (req, res) =>{
    User.findById({_id:req._id},
        (err,user) => {
            if (err) res.status(400).json({message:'Error al encontrar usuario'});
            else if (!user) res.status(400).json({message:'Error al encontrar usuario'});
            else if (user.services.tolls) {
                peaje.find({deleted:{$in: [false, null]}}).then((devices)=>{
                    res.send(devices);
                });
            }
            else res.status(400).json({message:'Error al encontrar dispositivo de peajes'});
        }
    )
};

let edit = (req, res) =>{
    User.findById({_id:req._id},
        (err,user) => {
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.tolls && user.roles.devices) {
                const myQuery = {_id: req.body.id};
                const newValues = {
                    $set: {
                        name: req.body.data.name,
                        system: req.body.data.system,
                        lat: req.body.data.lat,
                        long: req.body.data.long,
                        type: req.body.data.type,
                        idSecuros: req.body.data.idSecuros,
                        ip: req.body.data.ip,
                        ipSw: req.body.data.ipSw,
                        ipCam: req.body.data.ipCam,
                        ipNvr: req.body.data.ipNvr,
                        ipGateway: req.body.data.ipGateway
                    }
                };
                peaje.findOneAndUpdate(myQuery, newValues, (err, doc) => {
                    if (err) res.status(400).json({message: 'Error al editar el dispositivo de peajes'});
                    if (!doc) res.status(400).json({message: 'Error al editar el dispositivo de peajes'});
                    else{
                        ctrlLog.saveLog(doc.Email,"Edición de dispositivo de peajes");
                        res.status(200).json({message: 'Procedimiento correcto'});
                    }
                });
            }
            else res.status(400).json({message:'Error al editar dispositivo de peajes'});
        }
    )
};

let create = (req, res ) => {
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.tolls && user.roles.devices){
                let device = new peaje({
                    name: req.body.name,
                    system: req.body.system,
                    lat: req.body.lat,
                    long: req.body.long,
                    type: req.body.type,
                    idSecuros: req.body.idSecuros || '',
                    ip: req.body.ip || '',
                    ipSw: req.body.ipSw || '',
                    ipCam: req.body.ipCam || '',
                    ipNvr: req.body.ipNvr  || '',
                    ipGateway: req.body.ipGateway  || '',
                    state: {
                        ip: false,
                        ipSw: false,
                        ipCam: false,
                        ipNvr: false,
                        ipGateway: false
                    },
                });
                device.save((err) => {
                    if (err) res.status(400).json({message:'Error al crear dispositivo de peaje'});
                    else {
                        ctrlLog.saveLog(user.Email,"Creacion de dispositivo de peaje");
                        res.status(200).json({message:'Procedimiento correcto'});
                    }
                });
            }
            else res.status(400).json({message:'Error al editar dispositivo de peaje'});
        }
    );
};

let deleteDevice = (req, res ) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.lora && user.roles.devices){
                const myQuery = {_id: req.params.id};
                const newValues = {
                    $set: {
                        deleted: true,
                    }
                };
                peaje.findOneAndUpdate(myQuery, newValues, (err, ) => {
                    // As always, handle any potential errors:
                    if (err) return res.status(500).send(err);
                    ctrlLog.saveLog(user.Email,"Eliminación de dispositivo de peaje");
                    res.status(200).json({message: 'Procedimiento correcto'});
                });
            }
            else res.status(400).json({message:'Error al obtener usuarios'});
        }
    )
};

let maps = (req, res) => {
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.tolls) {
                sistema.find({}).then((systems)=>{
                    let urlRadio = systems.filter((key) => key.name === 'Radio')[0].url;
                    let urlSwitch = systems.filter((key) => key.name === 'Switch')[0].url;
                    let urlCamara = systems.filter((key) => key.name === 'Cámara')[0].url;
                    let urlNvr = systems.filter((key) => key.name === 'NVR')[0].url;
                    let urlGateway = systems.filter((key) => key.name === 'Gateway')[0].url;
                    peaje.find({}).then((devices)=>{
                        let final=[];
                        devices.forEach((device)=>{
                            if(device.ipCam && device.ipCam !== ""){
                                let url = '';
                                let fecha = '';
                                if(device.state.ipCam) url=`${urlCamara}_on.svg`;
                                else url=`${urlCamara}_off.svg`;
                                if (device.last.ipCam){
                                    let date1 = new Date(device.last.ipCam);
                                    fecha = `<h5><strong>Ultima conexión:</strong> ${date1.toLocaleString('en-GB')}</h5>`;
                                }
                                let popOver = {
                                    name:device.name,
                                    lat:device.lat,
                                    long:device.long,
                                    message:`<h4><strong>${device.name}</strong></h4><h5><strong>Dispositivo:</strong> Cámara</h5><h5><strong>Sistema:</strong>${device.system}</h5><h5><strong>ID securos:</strong>${device.idSecuros}</h5>${fecha}<h5><strong>Url:</strong><a href=http://${device.ipCam} target=_blank>${device.ipCam}</a></h5>`,
                                    type: "Cámara",
                                    state:device.state.ipCam,
                                    key:'tolls',
                                    url:url,
                                    id:`${device._id}camara`
                                };
                                final.push(popOver);
                            }
                            if(device.ip && device.ip !== ""){
                                let url = '';
                                let fecha = '';
                                if(device.state.ip) url=`${urlRadio}_on.svg`;
                                else url=`${urlRadio}_off.svg`;
                                if (device.last.ipCam){
                                    let date1 = new Date(device.last.ip);
                                    fecha = `<h5><strong>Ultima conexión:</strong> ${date1.toLocaleString('en-GB')}</h5>`;
                                }
                                let popOver ={
                                    name:device.name,
                                    lat:device.lat,
                                    long:device.long,
                                    message:`<h4><strong>${device.name}</strong></h4><h5><strong>Dispositivo:</strong> Radio</h5><h5><strong>Sistema:</strong>${device.system}</h5>${fecha}<h5><strong>Url:</strong><a href=http://${device.ip} target=_blank>${device.ip}</a></h5>`,
                                    type: "Radio",
                                    state:device.state.ip,
                                    key:'tolls',
                                    url:url,
                                    id:`${device._id}radio`
                                };
                                final.push(popOver);
                            }
                            if(device.ipSw && device.ipSw !== ""){
                                let url = '';
                                let fecha = '';
                                if(device.state.ipSw) url=`${urlSwitch}_on.svg`;
                                else url=`${urlSwitch}_off.svg`;
                                if (device.last.ipCam){
                                    let date1 = new Date(device.last.ipSw);
                                    fecha = `<h5><strong>Ultima conexión:</strong> ${date1.toLocaleString('en-GB')}</h5>`;
                                }
                                let popOver ={
                                    name:device.name,
                                    lat:device.lat,
                                    long:device.long,
                                    message:`<h4><strong>${device.name}</strong></h4><h5><strong>Dispositivo:</strong> Switch</h5><h5><strong>Sistema:</strong>${device.system}</h5>${fecha}<h5><strong>Url:</strong><a href=http://${device.ipSw} target=_blank>${device.ipSw}</a></h5>`,
                                    type: "Switch",
                                    state:device.state.ipSw,
                                    key:'tolls',
                                    url:url,
                                    id:`${device._id}switch`
                                };
                                final.push(popOver);
                            }
                            if(device.ipNvr && device.ipNvr !== ""){
                                let url = '';
                                let fecha = '';
                                if(device.state.ipNvr) url=`${urlNvr}_on.svg`;
                                else url=`${urlNvr}_off.svg`;
                                if (device.last.ipNvr){
                                    let date1 = new Date(device.last.ipSw);
                                    fecha = `<h5><strong>Ultima conexión:</strong> ${date1.toLocaleString('en-GB')}</h5>`;
                                }
                                let popOver ={
                                    name:device.name,
                                    lat:device.lat,
                                    long:device.long,
                                    message:`<h4><strong>${device.name}</strong></h4><strong>Dispositivo:</strong> NVR</h5><h5><strong>Sistema:</strong>${device.system}</h5>${fecha}<h5><strong>Url:</strong><a href=http://${device.ipNvr} target=_blank>${device.ipNvr}</a></h5>`,
                                    type: "NVR",
                                    state:device.state.ipNvr,
                                    key:'tolls',
                                    url:url,
                                    id:`${device._id}nvrpeaje`
                                };
                                final.push(popOver);
                            }
                            if(device.ipGateway && device.ipGateway!==""){
                                let url = '';
                                let fecha = '';
                                if(device.state.ipGateway) url=`${urlGateway}_on.svg`;
                                else url=`${urlGateway}_off.svg`;
                                if (device.last.ipGateway){
                                    let date1 = new Date(device.last.ipSw);
                                    fecha = `<h5><strong>Ultima conexión:</strong> ${date1.toLocaleString('en-GB')}</h5>`;
                                }
                                let popOver ={
                                    name:device.name,
                                    lat:device.lat,
                                    long:device.long,
                                    message:`<h4><strong>${device.name}</strong></h4><h5><strong>Dispositivo:</strong> Gateway</h5><h5><strong>Sistema:</strong>${device.system}</h5>${fecha}<h5><strong>Url:</strong><a href=http://${device.ipGateway} target=_blank>${device.ipGateway}</a></h5>`,
                                    type: "Gateway",
                                    state:device.state.ipGateway,
                                    key:'tolls',
                                    url:url,
                                    id:`${device._id}gateway`
                                };
                                final.push(popOver)
                            }
                        });
                        res.send(final);
                    });
                });
            }
            else res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
        }
    )
};


module.exports = {
    devices,
    edit,
    create,
    deleteDevice,
    maps
};

