const mongoose = require('mongoose');
const passport = require('passport');
let Roles = mongoose.model('Roles');
let Service = mongoose.model('Service');
const User = mongoose.model('User');
const sendMail = require('../email/emailHandler');
const ctrlLog  = require('../controllers/log.controller');

let authenticate = (req, res ) => {
    // call for passport authentication
    passport.authenticate('local', async (err, user, info) => {
        // error from passport middleware
        if (err) return res.status(400).json(err);
        // registered user
        else if (user) return res.status(200).json({ "token": user.generateJwt(),"name":user.UserName,"changePsw":user.changePWS,"roles": await generateRoles(user.roles), "sessionTime": user.time, "devices": await generateDevices(user.services) });
        // unknown user or wrong password
        else return res.status(404).json({message:info});
    })(req, res);
};

let changePsw = (req, res ) => {
    if (req.body.oldPassword && req.body.newPassword){
        User.findById({_id:req._id},
            (err,user)=>{
                //  error  mongoDB
                if (err) res.status(400).json({message:'Error al cambiar la clave'});
                //  no user
                else if (!user) res.status(400).send({message:'Error al cambiar la clave'});
                else {
                    if (user.verifyPassword(req.body.newPassword) ){
                        if(user.verifyPassword(req.body.oldPassword)) res.status(409).send({message:'Contraseña invalida'});
                        else res.status(404).send({message:'Error al cambiar la clave'});
                    }
                    // password valid
                    else if (user.verifyPassword(req.body.oldPassword)){
                        user.UserPassword = req.body.newPassword;
                        user.changePWS = false;
                        user.save((err,doc)=>{
                            if (err)
                                // error to change password
                                res.status(400).send({message:'Error al cambiar la clave'});
                            else{
                                // success to change password
                                sendMail.changePwsEmail(user);
                                ctrlLog.saveLog(user.Email,"Cambio de contraseña");
                                res.status(200).json({message:'Procedimiento correcto'});
                            }
                        });
                    }
                    // Password invalid
                    else{
                        res.status(404).json({message:'Contraseña invalida'});
                    }
                }
            });
    }
    else{
        res.status(400).json({message:'Error al cambiar la clave'});
    }
};

let forgotPsw = (req, res) => {
    if (req.body.email){
        User.findOne({ Email: req.body.email},
            (err, user)=>{
                // Error mongoDB
                if (err) res.status(400).json({message:'Error al cambiar la clave'});
                // There isn't user with that email address
                else if (!user) res.status(400).send({message:'Error al cambiar la clave'});
                else{
                    const newPsw = generatePassword();
                    user.UserPassword = newPsw;
                    user.cambio = true;
                    user.save((err,doc)=>{
                        if (err) res.status(400).send({message:'Error al cambiar la clave'});
                        else{
                            ctrlLog.saveLog(user.Email,"Recuperación de contraseña");
                            sendMail.forgotPwsEmail(user, newPsw);
                            res.status(200).json({message:'Procedimiento correcto'});
                        }
                    });
                }
            })
    }
    else{
        // no cumple con la estructura para
        return res.status(400).json({message:'Error al cambiar la clave'});
    }
};

let getUsers = (req, res ) => {
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if ( user.roles.users) {
                User.find({}, { UserName: 1, Email: 1, roles:1, time:1, services:1 })
                    .then((users) => {
                        res.send(users);
                    }
                );
            }
            else res.status(400).json({message:'Error al obtener usuarios'});
        }
    )
};

let modifyUsers = (req, res ) => {
    User.findById({_id:req._id},
        (err,user)=> {
            if (err) res.status(400).json({message: 'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message: 'Error al encontrar ususario'});
            else if (user.roles.users) {
                const myQuery = {_id: req.body._id};
                const newValues = {
                    $set: {
                        UserName: req.body.userName,
                        Email: req.body.email,
                        roles: {
                            maps: req.body.access.maps,
                            users: req.body.access.users,
                            devices: req.body.access.devices,
                            icons: req.body.access.icons,
                            geofence: req.body.access.geofence,
                            alerts: req.body.access.alerts,
                            reports: req.body.access.reports,
                            coverage: req.body.access.coverage,
                            applications: req.body.access.applications
                        },
                        time: req.body.time,
                        services: {
                            lora: req.body.devices.lora,
                            tolls: req.body.devices.tolls,
                            mina: req.body.devices.mina,
                            radios: req.body.devices.radios,
                            tracking: req.body.devices.tracking
                        }
                    }
                };
                User.findOneAndUpdate(myQuery, newValues, (err, doc) => {
                    if (err) res.status(400).json({message: 'Error al editar el usuario'});
                    if (!doc) res.status(400).json({message: 'Error al editar el usuario'});
                    else{
                        ctrlLog.saveLog(doc.Email,"Modificación de usuario");
                        sendMail.changeUserEmail(doc);
                        res.status(200).json({message: 'Procedimiento correcto'});
                    }
                });
            }
            else res.status(400).json({message: 'Error al obtener usuarios'});
        }
    )
};

let newUsers = (req, res ) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if ( user.roles.users) {
                let newPws =generatePassword();
                let newUser = new User({
                    UserName: req.body.userName,
                    Email: req.body.email,
                    roles:{
                        maps: req.body.access.maps,
                        users: req.body.access.users,
                        devices: req.body.access.devices,
                        icons: req.body.access.icons,
                        geofence: req.body.access.geofence,
                        alerts: req.body.access.alerts,
                        reports: req.body.access.reports,
                        coverage: req.body.access.coverage,
                        applications: req.body.access.applications
                    },
                    time: req.body.time,
                    services: {
                        lora: req.body.devices.lora,
                        tolls: req.body.devices.tolls,
                        mina: req.body.devices.mina,
                        radios: req.body.devices.radios,
                        tracking: req.body.devices.tracking
                    },
                    changePWS: true,
                    UserPassword: newPws
                });
                User.findOne({ Email: newUser.Email},
                    (err, user)=>{
                        // mongoDB error
                        if (err) res.status(400).json({message:'Error al crear el usuario'});
                        else if (!user){
                            newUser.save((err,doc)=>{
                                if (err) res.status(400).json({message:'Error al crear el usuario'});
                                else{
                                    ctrlLog.saveLog(doc.Email,"Nuevo usuario");
                                    sendMail.newUserEmail(doc, newPws);
                                    res.status(200).json({message: 'Procedimiento correcto'});
                                }
                            })
                        }
                        //there isn't user with that email
                        else res.status(409).json({message:'Error al cambiar la clave'});
                    })
            }
            else{
                res.status(400).json({message:'Error al obtener usuarios'});
            }
        }
    )
};

let deleteUser = (req, res ) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if ( user.roles.users ) {
                if (req.body.id !== req._id){
                    User.findOneAndRemove({_id: req.body.id}, (err, userDeleted) => {
                        // As always, handle any potential errors:
                        if (err) return res.status(500).send(err);
                        ctrlLog.saveLog(userDeleted.Email,"Usuario Eliminado");
                        sendMail.deleteUserEmail(userDeleted);
                        return res.status(200).json({message: 'Procedimiento correcto'});
                    });
                }
                else res.status(400).json({message:'No se puede elminar el usuario'});
            }
            else res.status(400).json({message:'Error al obtener usuarios'});
        }
    )
};

// other functions

let generateRoles = async (data) => {
    return await Roles.find({},{ _id: 0 })
        .then( async ( roles) => {
            // No roles in mongoDB
            if (!roles) return [];
            else {
                let rolesUser =[];
                roles.forEach( role =>{
                    if (data[role.name] === true){
                        rolesUser.push(Buffer.from(`USER_ROL_${role.key}`).toString('base64'))
                    }
                });
                return rolesUser
            }
        });
};
let generateDevices = async (data) => {
    return await Service.find({},{ _id: 0 })
        .then( async ( services) => {
            // No roles in mongoDB
            if (!services) return [];
            else {
                let rolesUser =[];
                services.forEach( service =>{
                    if (data[service.name] === true){
                        rolesUser.push(Buffer.from(`USER_DEV_${service.key}`).toString('base64'))
                    }
                });
                return rolesUser
            }
        });
};
let generatePassword = () => {
    let length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._*/+=?¡",
        retVal = "";
    for (let i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
};

module.exports = {
    authenticate,
    changePsw,
    forgotPsw,
    getUsers,
    modifyUsers,
    newUsers,
    deleteUser
};
