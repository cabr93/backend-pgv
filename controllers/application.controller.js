const mongoose = require('mongoose');
const applications = mongoose.model('applications');
const User = mongoose.model('User');
const ctrlLog  = require('../controllers/log.controller');

let getApplications = (req, res) =>{
    User.findById({_id:req._id},
        (err,user) => {
            if (err) res.status(400).json({message:'Error al encontrar usuario'});
            else if (!user) res.status(400).json({message:'Error al encontrar usuario'});
            // colocar condicion de rol como : user.roles.alerts
            else if (user.roles.applications) {
                applications.find().then((application)=>{
                    res.send(application);
                });
            }
            else res.status(400).json({message:'Error al encontrar puntos de covertura'});
        }
    )
};
let newApplication = (req, res) =>{
    User.findById({_id:req._id},
        (err, user) => {
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.roles.applications) {
                let application = new applications({
                    name : req.body.name,
                    url : req.body.url,
                    user : req.body.user,
                    password : req.body.password,
                    messageId : req.body.messageId
                });
                application.save((err) => {
                    if (err) res.status(400).json({message:'Error al crear applicacion'});
                    else{
                        ctrlLog.saveLog(user.Email,"Creacion de dispositivo Lora");
                        res.status(200).json({message:'Procedimiento correcto'});
                    }
                });
            }
            else res.status(400).json({message:'Error al crear apicacion'});
        }
    );
};


let deleteApplication = (req, res) => {
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar usuario'});
            else if (!user) res.status(400).json({message:'Error al encontrar usuario'});
            else if (user.roles.applications){
                const myQuery = {_id: req.params.id};
                applications.findByIdAndRemove(myQuery, (err, ) => {
                    // As always, handle any potential errors:
                    if (err) return res.status(500).send(err);
                    ctrlLog.saveLog(user.Email,"Eliminación de dispositivo Lora");
                    res.status(200).json({message: 'Procedimiento correcto'});
                });
            }
            else res.status(400).json({message:'Error al obtener usuarios'});
        }
    )
};


let editApplication = (req, res) =>{
    console.log(req.body)
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if ( user.roles.applications) {
                const myQuery = {_id: req.body._id};
                const newValues = {
                    $set: {
                        name: req.body.name,
                        url: req.body.url,
                        user: req.body.user,
                        password: req.body.password,
                        messageId: req.body.messageId
                    }
                };
                applications.findOneAndUpdate(myQuery, newValues, (err, doc) => {
                    if (err) res.status(400).json({message: 'Error al editar aplicacion'});
                    if (!doc) res.status(400).json({message: 'Error al editar applicacion'});
                    else{
                        res.status(200).json({message: 'Procedimiento correcto'});
                    }
                });
            }
            else res.status(400).json({message:'Error al editar applicacion'});
        }
    )
};

module.exports = {
    editApplication,
    getApplications,
    newApplication,
    deleteApplication
};
