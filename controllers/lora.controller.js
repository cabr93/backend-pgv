const mongoose = require('mongoose');
const Lora = mongoose.model('lora');
const User = mongoose.model('User');
const ctrlLog  = require('../controllers/log.controller');
const sistema = mongoose.model('sistema');

let devices = (req, res) =>{
    User.findById({_id:req._id},
        (err,user) => {
            if (err) res.status(400).json({message:'Error al encontrar usuario'});
            else if (!user) res.status(400).json({message:'Error al encontrar usuario'});
            else if (user.services.lora) {
                Lora.find({deleted:{$in: [false, null]}}).then((devices)=>{
                    res.send(devices);
                });
            }
            else res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
        }
    )
};

let edit = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.lora && user.roles.devices) {
                const myQuery = {_id: req.body.id};
                const newValues = {
                    $set: {
                        name: req.body.data.name,
                        brand: req.body.data.brand,
                        model: req.body.data.model,
                        lat: req.body.data.lat,
                        long: req.body.data.long,
                        EUI: req.body.data.eui,
                        ip: req.body.data.ip,
                        description: req.body.data.description,
                    }
                };
                Lora.findOneAndUpdate(myQuery, newValues, (err, doc) => {
                    if (err) res.status(400).json({message: 'Error al editar el dispositivo LoRa'});
                    if (!doc) res.status(400).json({message: 'Error al editar el dispositivo LoRa'});
                    else{
                        ctrlLog.saveLog(doc.Email,"Edición de dispositivo Lora");
                        res.status(200).json({message: 'Procedimiento correcto'});
                    }
                });
            }
            else res.status(400).json({message:'Error al editar dispositivo LoRa'});
        }
    )
};

let create = (req, res ) => {
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.lora && user.roles.devices){
                let device = new Lora({
                    name: req.body.name,
                    brand: req.body.brand,
                    model: req.body.model,
                    lat: req.body.lat,
                    long: req.body.long,
                    EUI: req.body.eui,
                    description: req.body.description,
                    ip: req.body.ip,
                    state: {
                        lora: false,
                        backhaul: false
                    },
                });
                device.save((err) => {
                    if (err) res.status(400).json({message:'Error al crear dispositivo lora'});
                    else{
                        ctrlLog.saveLog(user.Email,"Creacion de dispositivo Lora");
                        res.status(200).json({message:'Procedimiento correcto'});
                    }
                });
            }
            else res.status(400).json({message:'Error al editar dispositivo LoRa'});
        }
    );
};

let deleteDevice = (req, res, next) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.lora && user.roles.devices){
                const myQuery = {_id: req.params.id};
                const newValues = {
                    $set: {
                        deleted: true,
                    }
                };
                Lora.findOneAndUpdate(myQuery, newValues, (err, ) => {
                    // As always, handle any potential errors:
                    if (err) return res.status(500).send(err);
                    ctrlLog.saveLog(user.Email,"Eliminación de dispositivo Lora");
                    res.status(200).json({message: 'Procedimiento correcto'});
                });
            }
            else res.status(400).json({message:'Error al obtener usuarios'});
        }
    )
};

let maps = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.lora) {
                let final = [];
                sistema.find({}).then((systems)=>{
                    let urlLora = systems.filter((key) => key.name === 'LoRaWAN')[0].url;
                    let urlLoraBack = systems.filter((key) => key.name === 'LoRaWAN backhaul')[0].url;
                    Lora.find({}).then((devices)=>{
                        devices.forEach((device)=>{
                            let url = '';
                            if (device.state.lora) url=`${urlLora}_on.svg`;
                            else url=`${urlLora}_off.svg`;
                            let date1 = new Date(device.last);
                            let popOver = {
                                name:device.name,
                                lat:device.lat,
                                long:device.long,
                                message:`<h4><strong>${device.name}</strong></h4><h5><strong>Dispositivo:</strong> LoRa</h5><h5><strong>Conexión:</strong> LoRaWAN</h5><h5><strong>Ultima conexión:</strong> ${date1.toLocaleString('en-GB')}</h5><h5><strong>Marca:</strong>${device.brand}</h5><h5><strong>Modelo:</strong>${device.model}</h5><h5><strong>Url:</strong><a href=http://${device.ip} target=_blank>${device.ip}</a></h5><h5>${device.description}</h5>`,
                                type:'LoRaWAN',
                                state:device.state.lora,
                                key:'lora',
                                url: url,
                                id: `${device._id}LoRaWAN`
                            };
                            final.push(popOver);
                            if(device.state.backhaul) url = `${urlLoraBack}_on.svg`;
                            else url=`${urlLoraBack}_off.svg`;
                            popOver = {
                                ...popOver,
                                message:`<h4><strong>${device.name}</strong></h4><h5><strong>Dispositivo:</strong> LoRa</h5><h5><strong>Conexión:</strong> Backhaul</h5><h5><strong>Ultima conexión:</strong> ${date1.toLocaleString('en-GB')}</h5><h5><strong>Marca:</strong>${device.brand}</h5><h5><strong>Modelo:</strong>${device.model}</h5><h5><strong>Url:</strong><a href=http://${device.ip} target=_blank>${device.ip}</a></h5><h5>${device.description}</h5>`,
                                type:'LoRaWAN backhaul',
                                state:device.state.backhaul,
                                url: url,
                                id: `${device._id}LoRaWANback`
                            };
                            final.push(popOver)
                        });
                        res.send(final);
                    });
                });
            }
            else{
                res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
            }
        }
    )
};

module.exports = {
    devices,
    edit,
    create,
    deleteDevice,
    maps
};

