const mongoose = require('mongoose');
const radio = mongoose.model('radio');
const User = mongoose.model('User');
const ctrlLog  = require('../controllers/log.controller');
const sistema = mongoose.model('sistema');

let devices = (req, res) =>{
    User.findById({_id:req._id},
        (err,user) => {
            if (err) res.status(400).json({message:'Error al encontrar usuario'});
            else if (!user) res.status(400).json({message:'Error al encontrar usuario'});
            else if (user.services.radios) {
                radio.find({deleted:{$in: [false, null]}}).then((devices)=>{
                    res.send(devices);
                });
            }
            else res.status(400).json({message:'Error al encontrar dispositivo de Radio'});
        }
    )
};

let edit = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.radios && user.roles.devices) {
                const myQuery = {_id: req.body.id};
                const newValues = {
                    $set: {
                        name: req.body.data.name,
                        system: req.body.data.system,
                        stretch: req.body.data.stretch,
                        lat: req.body.data.lat,
                        long: req.body.data.long,
                        brand: req.body.data.brand,
                        model: req.body.data.model,
                        type: req.body.data.type,
                        ip: req.body.data.ip,
                        description: req.body.data.description,
                    }
                };
                radio.findOneAndUpdate(myQuery, newValues, (err, doc) => {
                    if (err) res.status(400).json({message: 'Error al editar el dispositivo de radio'});
                    if (!doc) res.status(400).json({message: 'Error al editar el dispositivo de radio'});
                    else{
                        ctrlLog.saveLog(doc.Email,"Edición de dispositivo de radio");
                        res.status(200).json({message: 'Procedimiento correcto'});
                    }
                });
            }
            else res.status(400).json({message:'Error al editar dispositivo Radio'});
        }
    )

};

let create = (req, res) =>{
    User.findById({_id:req._id},
        (err,user) => {
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.radios && user.roles.devices) {
                let device = new radio({
                    name: req.body.name,
                    system: req.body.system,
                    stretch: req.body.stretch,
                    lat: req.body.lat,
                    long: req.body.long,
                    description: req.body.description,
                    brand: req.body.brand,
                    model: req.body.model,
                    type: req.body.type,
                    ip: req.body.ip,
                    state: false
                });
                device.save((err )=>{
                    if (err) res.status(400).json({message:'Error al editar dispositivo Radio'});
                    else{
                        ctrlLog.saveLog(user.Email,"Creacion de dispositivo de Radio");
                        res.status(200).json({message:'Procedimiento correcto'});
                    }
                })
            }
            else res.status(400).json({message:'Error al editar dispositivo Radio'});
        }
    )
};

let deleteDevice = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.radios && user.roles.devices){
                const myQuery = {_id: req.params.id};
                const newValues = {
                    $set: {
                        deleted: true,
                    }
                };
                radio.findOneAndUpdate(myQuery, newValues, (err, ) => {
                    // As always, handle any potential errors:
                    if (err) return res.status(500).send(err);
                    ctrlLog.saveLog(user.Email,"Eliminación de dispositivo de Radio");
                    res.status(200).json({message: 'Procedimiento correcto'});
                });
            }
            else res.status(400).json({message:'Error al obtener usuarios'});
        }
    )
};

let maps = (req, res) =>{
    User.findById({_id:req._id},
        (err,user)=>{
            if (err) res.status(400).json({message:'Error al encontrar ususario'});
            else if (!user) res.status(400).json({message:'Error al encontrar ususario'});
            else if (user.services.radios) {
                let final = [];
                sistema.find({}).then((systems)=>{
                    let urlRadio = systems.filter((key) => key.name === 'Radio')[0].url;
                    let urlSwitch = systems.filter((key) => key.name === 'Switch')[0].url;
                    radio.find({}).then((devices)=>{
                        devices.forEach((device)=>{
                            let url = '';
                            switch (device.type) {
                                case 'Radio':
                                    url = urlRadio;
                                    break;
                                case 'Switch':
                                    url = urlSwitch;
                                    break;
                                default:
                                    break;
                            }
                            if(device.state) url=`${url}_on.svg`;
                            else url=`${url}_off.svg`;
                            let fecha = '';
                            if (device.last){
                                let date1 = new Date(device.last);
                                fecha = `<h5><strong>Ultima conexión:</strong> ${date1.toLocaleString('en-GB')}</h5>`;
                            }
                            let popOver = {
                                name:device.name,
                                lat:device.lat,
                                long:device.long,
                                message:`<h4><strong>${device.name}</strong></h4><h5><strong>Sistema:</strong>${device.system}</h5><h5><strong>Tramo:</strong>${device.stretch}</h5>${fecha}<h5><strong>Marca:</strong>${device.brand}</h5><h5><strong>Modelo:</strong>${device.model}</h5><h5><strong>Url:</strong><a href=http://${device.ip} target=_blank>${device.ip}</a></h5><h5>${device.description}</h5>`,
                                type: device.type,
                                state:device.state,
                                key:'radios',
                                url:url,
                                id: `${device._id}${device.type}`
                            };
                            final.push(popOver);
                        });
                        res.send(final);
                    });
                });
            }
            else res.status(400).json({message:'Error al encontrar dispositivo LoRa'});
        }
    )
};

module.exports = {
    devices,
    edit,
    create,
    deleteDevice,
    maps
};
