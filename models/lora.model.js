const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let loraSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    brand: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    lat: {
        type: String,
        required: true
    },
    long: {
        type: String,
        required: true,
    },
    state: {
        backhaul: Boolean,
        lora: Boolean
    },
    description: {
        type: String,
        required: true,
    },
    ip: {
        type: String,
        required: true,
    },
    EUI: {
        type: String,
        required: true,
    },
    last:{
        type:Date,
        required: false,
    },
    deleted:{
        type: Boolean,
        required: false,
        default: false
    }
});

mongoose.model('loraInf', new Schema(), 'loraInf');
mongoose.model('lora', loraSchema);