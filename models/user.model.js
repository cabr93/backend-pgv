const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
let Schema = mongoose.Schema;


let userSchema = new Schema({
    UserName: {
        type: String,
        required: true
    },
    UserPassword: {
        type: String,
        required: true,
        minlength : 6
    },
    Email: {
        type: String,
        required: true,
        unique: true
    },
    SaltSecret: {
        type: String,
        required: false
    },
    changePWS: {
        type: Boolean,
        required: true,
        default: true
    },
    time: {
        type: Number,
        required: true,
        default: 30
    },
    roles: {
        maps: Boolean,
        users: Boolean,
        services: Boolean,
        icons: Boolean,
        geofence: Boolean,
        alerts: Boolean,
        reports: Boolean,
        coverage: Boolean,
        devices: Boolean,
        applications: Boolean,
    },
    services: {
        lora: Boolean,
        tolls: Boolean,
        mina: Boolean,
        radios: Boolean,
        tracking: Boolean
    }
});

// Custom validation for email
userSchema.path('Email').validate((val) => {
    let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');

// Events
userSchema.pre('save', function (next) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.UserPassword, salt, (err, hash) => {
            this.UserPassword = hash;
            this.SaltSecret = salt;
            next();
        });
    });
});

// Methods
userSchema.methods.verifyPassword = function (UserPassword) {
    return bcrypt.compareSync(UserPassword, this.UserPassword);
};

userSchema.methods.generateJwt = function () {
    return jwt.sign({ _id: this._id},
        process.env.JWT_SECRET,
        {
            expiresIn: process.env.JWT_EXP
        });
};

mongoose.model('User', userSchema);
