const mongoose = require('mongoose');
let Schema = mongoose.Schema;


let radioSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    system: {
        type: String,
        required: true
    },
    stretch: {
        type: String,
        required: true
    },
    lat: {
        type: String,
        required: true
    },
    long: {
        type: String,
        required: true,
    },
    state: {
        type: Boolean,
        required: false,
    },
    description: {
        type: String,
        required: true,
    },
    brand: {
        type: String,
        required: true,
    },
    model: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
    },
    ip: {
        type: String,
        required: true,
    },
    last:{
        type:Date,
        required: false,
    },
    deleted:{
        type: Boolean,
        required: false,
        default: false
    }
});


mongoose.model('radiosInf', new Schema(), 'radiosInf');

mongoose.model('radio', radioSchema);