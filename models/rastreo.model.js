const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const rastreosSchema = new Schema({
    placa: {
        type: String,
        required: true
    },
    lastubication: {
        type: Date,
        required: true
    },long: {
        type: String,
        required: true
    },lat: {
        type: String,
        required: true
    },
    speed: {
        type: String,
        required: true
    },
    state: {
        type: Boolean,
        required: false
    }
});

mongoose.model('rastreo', rastreosSchema);

