const mongoose = require('mongoose');


const logSchema = new mongoose.Schema({
    hour: {
        type: Date,
        required: true
    },
    responsible: {
        type: String,
        required: true
    },
    action: {
        type: String,
        required: true
    }
});

mongoose.model('log', logSchema);

