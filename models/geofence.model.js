const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let geofenceSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    devices: {
        type: Array,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    coords: {
        type: Array,
        required: true
    }
});

mongoose.model('geocercas', geofenceSchema);