const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let coverageSchema = new Schema({
    lat: {
        type: String,
        required: true
    },
    lng: {
        type: String,
        required: true
    },
    devEui: {
        type: String,
        required: true
    },
    gwEui: {
        type: String,
        required: true
    },
    rssi: {
        type: String,
        required: true
    },
    lSnr: {
        type: String,
        required: true
    },
    msgId: {
        type: String,
        required: true
    },
    freq: {
        type: String,
        required: true
    },
    time: {
        type: Date,
        required: true
    },
    payload: {
        type: String,
        required: false
    }
});

mongoose.model('coverage', coverageSchema);
