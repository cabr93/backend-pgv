const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const sistemaSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    icon: {
        type: String,
        required: false
    },
    id:{
        type:String,
        required: false
    },
    url:{
        type:String,
        required: false
    }
});

mongoose.model('sistema', sistemaSchema);
