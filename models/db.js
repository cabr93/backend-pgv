const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex:true
}, (err, res) => {
    if (err) throw err;
    console.log('Base de datos online')
});

require('./user.model');
require('./role.model');
require('./service.model');
require('./log.model');
require('./lora.model');
require('./sistema.model');
require('./radio.model');
require('./peajes.model');
require('./mina.model');
require('./rastreo.model');
require('./geofence.model');
require('./email.model');
require('./coverage.model');
require('./application.model');
