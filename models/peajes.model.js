const mongoose = require('mongoose');
let Schema = mongoose.Schema;


let peajeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    system: {
        type: String,
        required: true
    },
    lat: {
        type: String,
        required: true
    },
    long: {
        type: String,
        required: true,
    },
    idSecuros: {
        type: String,
        required: false,
    },
    ip: {
        type: String,
        required: false,
    },
    ipSw: {
        type: String,
        required: false,
    },
    ipCam: {
        type: String,
        required: false,
    },
    ipNvr: {
        type: String,
        required: false,
    },
    ipGateway: {
        type: String,
        required: false,
    },
    state: {
        ip: {
            type: Boolean,
            required: false,
        },
        ipSw: {
            type: Boolean,
            required: false,
        },
        ipCam: {
            type: Boolean,
            required: false,
        },
        ipNvr: {
            type: Boolean,
            required: false,
        },
        ipGateway: {
            type: Boolean,
            required: false,
        }
    },
    last: {
        ip: {
            type: Date,
            required: false,
        },
        ipSw: {
            type: Date,
            required: false,
        },
        ipCam: {
            type: Date,
            required: false,
        },
        ipNvr: {
            type: Date,
            required: false,
        },
        ipGateway: {
            type: Date,
            required: false,
        }
    },
    deleted:{
        type: Boolean,
        required: false,
        default: false
    }
});

mongoose.model('peajesInf', new Schema(), 'peajesInf');

mongoose.model('peaje', peajeSchema);