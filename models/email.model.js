const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let emailSchema = new Schema({
    email: {
        type: String,
        required: true
    }
});

mongoose.model('email', emailSchema);