const mongoose = require('mongoose');


const applicationSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    user: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    messageId: {
        type: String,
        required: true
    }
});

mongoose.model('applications', applicationSchema);

