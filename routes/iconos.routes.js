const express = require('express');
const router = express.Router();

const jwtHelper = require('../middlewares/jwtHelper');
const ctrlIcons = require('../controllers/iconos.controller');

router.get('/iconos', jwtHelper.verifyJwtToken, ctrlIcons.icons);
router.post('/iconosedit', jwtHelper.verifyJwtToken, ctrlIcons.edit);


module.exports = router;
