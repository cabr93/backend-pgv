const express = require('express');
const router = express.Router();

const jwtHelper = require('../middlewares/jwtHelper');
const ctrlReports = require('../controllers/report.controller');

router.post('/report', jwtHelper.verifyJwtToken, ctrlReports.reports);

module.exports = router;
