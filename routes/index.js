const express = require('express');
const app = express();

app.use('/api', require('./user.routes'));
app.use('/api', require('./lora.routes'));
app.use('/api', require('./radio.routes'));
app.use('/api', require('./peajes.routes'));
app.use('/api', require('./mina.routes'));
app.use('/api', require('./rastreo.routes'));
app.use('/api', require('./iconos.routes'));
app.use('/api', require('./logs.routes'));
app.use('/api', require('./reportes.routes'));
app.use('/api', require('./geofence.routes'));
app.use('/api', require('./coverage.routes'));
app.use( '/api', require('./applications.routes'));

module.exports = app;
