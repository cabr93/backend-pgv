const express = require('express');
const router = express.Router();

const jwtHelper = require('../middlewares/jwtHelper');
const ctrlLora = require('../controllers/lora.controller');


router.get('/lora', jwtHelper.verifyJwtToken, ctrlLora.devices);
router.put('/lora', jwtHelper.verifyJwtToken, ctrlLora.edit);
router.post('/lora', jwtHelper.verifyJwtToken, ctrlLora.create);
router.delete('/lora/:id', jwtHelper.verifyJwtToken, ctrlLora.deleteDevice);

router.get('/loramaps', jwtHelper.verifyJwtToken, ctrlLora.maps);

module.exports = router;
