const express = require('express');
const router = express.Router();
const ctrlLog = require('../controllers/log.controller');
const jwtHelper = require('../middlewares/jwtHelper');


router.get('/logs', jwtHelper.verifyJwtToken, ctrlLog.logs);


module.exports = router;