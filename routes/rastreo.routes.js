const express = require('express');
const router = express.Router();

const jwtHelper = require('../middlewares/jwtHelper');
const ctrlRastreo = require('../controllers/rastreo.controller');

router.get('/rastreo', jwtHelper.verifyJwtToken, ctrlRastreo.devices);
router.get('/rastreomaps', jwtHelper.verifyJwtToken, ctrlRastreo.maps);

module.exports = router;
