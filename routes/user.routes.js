const express = require('express');
const router = express.Router();

const ctrlUser = require('../controllers/user.controller');
const jwtHelper = require('../middlewares/jwtHelper');

router.post('/authenticate', ctrlUser.authenticate);
router.post('/changepsw', jwtHelper.verifyJwtToken, ctrlUser.changePsw);
router.post('/forgotpsw', ctrlUser.forgotPsw);
router.get('/users', jwtHelper.verifyJwtToken, ctrlUser.getUsers);
// ================================
// Cambiar por put delete y post
// ================================
router.post('/modifyusers', jwtHelper.verifyJwtToken, ctrlUser.modifyUsers);
router.post('/newuser', jwtHelper.verifyJwtToken, ctrlUser.newUsers);
router.post('/userdelete', jwtHelper.verifyJwtToken, ctrlUser.deleteUser);

module.exports = router;
