const express = require('express');
const router = express.Router();
const jwtHelper = require('../middlewares/jwtHelper');
const ctrlApplications= require('../controllers/application.controller');

router.get('/applications', jwtHelper.verifyJwtToken, ctrlApplications.getApplications);
router.post('/applications', jwtHelper.verifyJwtToken, ctrlApplications.newApplication);
router.delete('/applications/:id', jwtHelper.verifyJwtToken, ctrlApplications.deleteApplication);
router.put('/applications', jwtHelper.verifyJwtToken, ctrlApplications.editApplication);

module.exports = router;
