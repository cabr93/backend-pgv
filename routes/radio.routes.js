const express = require('express');
const router = express.Router();

const jwtHelper = require('../middlewares/jwtHelper');
const ctrlRadio = require('../controllers/radio.controller');


router.get('/radio', jwtHelper.verifyJwtToken, ctrlRadio.devices);
router.put('/radio', jwtHelper.verifyJwtToken, ctrlRadio.edit);
router.post('/radio', jwtHelper.verifyJwtToken, ctrlRadio.create);
router.delete('/radio/:id', jwtHelper.verifyJwtToken, ctrlRadio.deleteDevice);
router.get('/radiomaps', jwtHelper.verifyJwtToken, ctrlRadio.maps);

module.exports = router;
