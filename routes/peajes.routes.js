const express = require('express');
const router = express.Router();

const jwtHelper = require('../middlewares/jwtHelper');
const ctrlPeaje = require('../controllers/peaje.controller');


router.get('/peaje', jwtHelper.verifyJwtToken, ctrlPeaje.devices);
router.put('/peaje', jwtHelper.verifyJwtToken, ctrlPeaje.edit);
router.post('/peaje', jwtHelper.verifyJwtToken, ctrlPeaje.create);
router.delete('/peaje/:id', jwtHelper.verifyJwtToken, ctrlPeaje.deleteDevice);
router.get('/peajemaps', jwtHelper.verifyJwtToken, ctrlPeaje.maps);

module.exports = router;