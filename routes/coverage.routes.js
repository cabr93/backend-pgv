const express = require('express');
const router = express.Router();
const jwtHelper = require('../middlewares/jwtHelper');
const ctrlCoverage= require('../controllers/coverage.controller');


router.get('/coverage', jwtHelper.verifyJwtToken, ctrlCoverage.coveragePoint);
router.get('/coveragegateways', jwtHelper.verifyJwtToken, ctrlCoverage.coverageGateways);



module.exports = router;