const express = require('express');
const router = express.Router();

const jwtHelper = require('../middlewares/jwtHelper');
const ctrlMina = require('../controllers/mina.controller');

router.get('/mina', jwtHelper.verifyJwtToken, ctrlMina.devices);
router.put('/mina', jwtHelper.verifyJwtToken, ctrlMina.edit);
router.post('/mina', jwtHelper.verifyJwtToken, ctrlMina.create);
router.delete('/mina/:id', jwtHelper.verifyJwtToken, ctrlMina.deleteDevice);
router.get('/minamaps', jwtHelper.verifyJwtToken, ctrlMina.maps);

module.exports = router;
