
const express = require('express');
const router = express.Router();
const ctrlGeo = require('../controllers/geofence.controller');
const jwtHelper = require('../middlewares/jwtHelper');
const ctrlEmail = require('../controllers/email.controller');


router.get('/geofence', jwtHelper.verifyJwtToken, ctrlGeo.getGeofence);
router.post('/geofence', jwtHelper.verifyJwtToken, ctrlGeo.geofenceAdd);
router.put('/geofence', jwtHelper.verifyJwtToken, ctrlGeo.geofenceEdit);
router.delete('/geofence/:id', jwtHelper.verifyJwtToken, ctrlGeo.deleteGeofence);
//
router.get('/geofencee', jwtHelper.verifyJwtToken, ctrlEmail.getEmail);
router.put('/geofencee', jwtHelper.verifyJwtToken, ctrlEmail.edit);



module.exports = router;