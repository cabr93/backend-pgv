const nodeMailer = require('nodemailer');
const fs = require('fs');

let sendEmail = (email, mensaje, asunto) => {
    // nodemailer stuff will go here
    let transporter = nodeMailer.createTransport({
        host: "smtp.office365.com", // hostname
        secureConnection: false, // TLS requires secureConnection to be false
        port: 587, // port for secure SMTP
        tls: {
            ciphers: 'SSLv3'
        },
        auth: {
            user: 'novedades@gruposims.com',
            pass: 'PlataformaGv2019'
        }
    });

    let mailOptions = {
        from: 'novedades@gruposims.com', // sender address (who sends)
        to: email, // list of receivers (who receives)
        subject: asunto, // Subject line
        text: asunto, // plaintext body
        html: mensaje + "<br>" //'<b>Hello world </b><br> This is the first email sent with Nodemailer in Node.js' // html body
    };

    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
            //res.send(500, err.message);
        } else {
            console.log("Email sent");
            //res.status(200).jsonp(req.body);
        }
    });
};

let sendReport = (email, message, mailSubject, file) => {
    let transporter = nodeMailer.createTransport({
        host: "smtp.office365.com", // hostname
        secureConnection: false, // TLS requires secureConnection to be false
        port: 587, // port for secure SMTP
        tls: {
            ciphers: 'SSLv3'
        },
        auth: {
            user: 'novedades@gruposims.com',
            pass: 'PlataformaGv2019'
        }
    });
    let mailOptions = {
        from: 'novedades@gruposims.com', // sender address (who sends)
        to: email, // list of receivers (who receives)
        subject: mailSubject, // Subject line
        text: mailSubject, // plaintext body
        html: message + "<br>", //'<b>Hello world </b><br> This is the first email sent with Nodemailer in Node.js' // html body
        attachments: [{
            filename: 'reporte.pdf',
            path: file,
            contentType: 'application/pdf'
        }]
    };
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log("Email sent");
            fs.unlinkSync(file);
        }
    });
};

module.exports = {
    sendEmail,
    sendReport
};