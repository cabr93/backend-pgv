const sender = require('./sender');

const confidentialMessage="<BR><BR><BR><strong>Cordialmente</strong>,<BR><BR>  <strong>Plataforma de gestion y visualización de dispositivos IoT del grupo SIMS</strong> <BR><BR> <strong>CONFIDENCIALIDAD:</strong> La información contenida en este mensaje y en los archivos adjuntos es confidencial, reservada y está dirigida exclusivamente a su destinatario, sin la intención de que sea conocida por terceros, por lo tanto, de conformidad con las normas legales vigentes, su interceptación, sustracción, extravío, reproducción, lectura o uso está prohibido a cualquier persona diferente. Si por error ha recibido este mensaje por favor discúlpenos, notifíquenoslo y elimínelo.";

let changePwsEmail = (user) => {
    const mailSubject = 'Cambio de clave';
    const message = "Estimado usuario <strong>"+user.UserName+"</strong>, <BR><BR><BR>Tu contraseña ha sido actualizada correctamente." + confidentialMessage;
    sender.sendEmail(user.Email, message, mailSubject);
};

let forgotPwsEmail = (user, psw) => {
    const mailSubject = 'Recuperación de contraseña';
    const message = "Estimado usuario <strong>"+user.UserName+"</strong>, <BR><BR><BR> Hemos recibido tu solicitud para restablecer la contraseña de tu cuenta <strong> Plataforma de gestion y visualización de dispositivos IoT del grupo SIMS</strong> . <BR><BR>  A continuación te brindamos una contraseña temporal de ingreso, una vez inicies sesión es obligatorio que la modifiques.<BR><BR><BR> <strong> su nueva clave es </strong>"+  psw + confidentialMessage;
    sender.sendEmail(user.Email, message, mailSubject);
};

let changeUserEmail = (user) => {
    const mailSubject = 'Cambio de permisos';
    const message = "Estimado usuario <strong>"+user.UserName+"</strong>, <BR><BR><BR>Los permisos de su usuario han sido modificados."+confidentialMessage;
    sender.sendEmail(user.Email, message, mailSubject);
};

let newUserEmail = (user, psw) => {
    const mailSubject = 'Usuario Nuevo';
    const message = "Estimado usuario <strong>"+user.UserName+"</strong>, <BR><BR><BR>Tu nueva cuenta de acceso en la Plataforma de Gestión y Visualización se encuentra habilitada. A continuación te brindamos una contraseña temporal de ingreso, una vez incies sesión es obligatorio que la modifiques.<BR> <BR><strong> La contraseña es :</strong>  "+ psw +confidentialMessage;
    sender.sendEmail(user.Email, message, mailSubject);
};


let deleteUserEmail = (user) => {
    const mailSubject = 'Usuario Eliminado';
    const message = "Estimado usuario <strong>"+user.UserName+"</strong>, <BR><BR><BR>!Tu cuenta se ha sido eliminada! <BR><BR> Si desea recuperar el usuario por favor comuniquese con el adminsitrador de la Plataforma.";
    sender.sendEmail(user.Email, message, mailSubject);
};

let reportEmail = (user, type, prefijo) => {
    const message  = `Estimado usuario <strong> ${user.UserName}</strong>, <BR><BR><BR>Adjunto encontrara el informe del sistema de ${type} .` + confidentialMessage;
    sender.sendReport(user.Email, message, `Informe del sistema de ${type}`, `C:/Users/Administrador/Documents/PGV3/salida${type}${prefijo}.pdf`);
};

module.exports = {
    changePwsEmail,
    forgotPwsEmail,
    changeUserEmail,
    newUserEmail,
    deleteUserEmail,
    reportEmail
};
